package Buscaminas;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicTabbedPaneUI.MouseHandler;

import Core.Board;
import Core.Window;


public class mainer {
	
	static Mesura m = new Mesura();
	static int mines = 10;
	static Scanner sc = new Scanner(System.in);
	static Board b = new Board();	
	static Window w = new Window(b);
	public static void main(String[] args) {
		System.out.println("Benvingut a BUSCAMINAS   >>>>     By Pol Gonzalo(Nohadon)");
		Posicio pos = new Posicio();
		Player p1 = new Player();
		menu(p1, pos);
	}

	/**
	 * 
	 * @param p1  El jugador que anirà a fer la partida i que s'ha configurat o no
	 *            prèviament el nom
	 * 
	 * @param pos Posició es la clase que guardarà per cada ronda les coordenades
	 * 
	 */
	public static void menu(Player p1, Posicio pos) {
		p1.name = "Player 1";
		p1.wins = 0;
		m.f = 9;
		m.c = 9;
		LinkedHashMap<String, Integer> ran = new LinkedHashMap<String, Integer>();
		ran.put(p1.name, p1.wins);
		int a = print_options();
		while (a != 0) {
			switch (a) {
			case 1:
				System.out.println();
				System.out.println();
				System.out.println("Opció 1: Mostrar ajuda");
				help(p1);
				break;
			case 2:
				System.out.println();
				System.out.println();
				System.out.println("Opció 2: Configuracions");
				config(p1);
				ran.put(p1.name, p1.wins);
				break;
			case 3:
				System.out.println();
				System.out.println();
				System.out.println("Opció 3: Vamo a juga *Procede a beber zumito*");
				jugar(pos, p1);
				ran.put(p1.name, p1.wins);
				break;
			case 4:
				System.out.println();
				System.out.println();
				System.out.println("Opció 4: Ranking");
				Ranking(ran);
				break;
			default:
				System.out.print("ADVERTENCIA: No has posat una opció válida, torn a posarla: ");
				break;
			}
			a = print_options();
		}
	}

	/**
	 * @param print Imprimeix totes les línies default del menú.
	 * @return La opció integer per al menú principal (funció menu())
	 */
	private static int print_options() {
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("<<<   MENU   >>>");
		System.out.println();
		System.out.println();
		System.out.println("Tens les següents opcions:  ");
		System.out.println("OPCIO 1: Mostrar ajuda");
		System.out.println("OPCIO 2: Opcions");
		System.out.println("OPCIO 3: Jugar Partida");
		System.out.println("OPCIO 4: Rankings");
		System.out.println("OPCIO 0: Sortir");
		System.out.println();
		System.out.print("Posa una opcio [0-4]  > ");
		int a = sc.nextInt();
		return a;

	}

	/**
	 * 
	 * @param p1 El jugador per a accedir al seu nom. Aquesta funció dona informació
	 *           al usuari sobre la partida actual
	 */
	public static void help(Player p1) {
		System.out.println();
		System.out.println();
		System.out.println("Taulell actual de: " + m.f + "x" + m.c);
		System.out.println("Nom del Jugador: " + p1.name);
		System.out.println("Nombre de mines actual: " + mines);
		System.out.println();
		System.out.println();
	}

	/**
	 * 
	 * @param p1 El jugador per a poder modificar el seu nom
	 */
	public static void config(Player p1) {
		sc.nextLine();
		System.out.println();
		System.out.println();
		System.out.print("Posa el nom del jugador que desitges: ");
		p1.name = sc.nextLine();
		p1.wins = 0;
		System.out.print("Tamany de files i columnes: ");
		m.f = sc.nextInt();
		m.c = m.f;
		boolean ey = false;
		while (!ey) {
			System.out.print("Posa el nombre de mines que desitges: ");
			mines = sc.nextInt();
			if (mines < (m.f * m.c)) {
				ey = true;
			} else {
				System.out.println("Marc deja de trolear anda");
				System.out.println("QUE NO TE DEJO PONER MÁS MINAS COÑO");
				System.out.println();
			}
		}
	}

	/**
	 * 
	 * @return El tauler intern que te guardat l'informació sobre la posicio de les
	 *         mines.
	 */
	public static int[][] generar_ti() {
		Random r = new Random();
		int[][] mtx = new int[m.f][m.c];
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				mtx[i][j] = 0;
			}
		}
		int count = 0;
		while (count != mines) {
			int f = r.nextInt(mtx.length);
			int c = r.nextInt(mtx[0].length);
			if (mtx[f][c] == 0) {
				mtx[f][c] = 1;
				count++;
			}
		}
		return mtx;
	}

	/**
	 * 
	 * @return El taulell exterior ple de '9' per a representar fitxes sense
	 *         descobrir i on modificarem els números.
	 */
	public static int[][] generar_te() {
		int[][] mtx = new int[m.f][m.c];
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				mtx[i][j] = 9;
			}
		}
		return mtx;
	}

	/**
	 * 
	 * @param mtx la matriu on volem comprova si surt.
	 * @param f   la posicio de fila.
	 * @param c   la posicio de columna.
	 * @return Un boleà que indica que la coordenada a la que s'intenta accedir està
	 *         fora de la matriu.
	 */
	public static boolean fora(int[][] mtx, int f, int c) {
		if (f > mtx.length - 1 || c > mtx.length - 1 || f < 0 || c < 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param mtx El taulell que volem mostrar per pantalla
	 * @return Un taulell per pantalla.
	 */
	public static void imprimir_tablero(int[][] mtx) {
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				System.out.print(mtx[i][j]);
			}
			System.out.println();
		}
		b.draw(mtx, 't'); //la t implica que és mode text.

	}

	/**
	 * 
	 * @param p classe pública que emmagatzema la informació de les coordenades que
	 *          demana l'usuari.
	 * @return la clàse pública posició amb les coordenades escollides per l'usuari.
	 */
	public static Posicio demana(Posicio p) {
		System.out.println();
		System.out.print("Posa la posicio de fila que vols [1-" + m.f + "]: ");
		p.f = sc.nextInt() - 1;
		System.out.print("Posa la posicio de columna que vols [1-" + m.f + "]: ");
		p.c = sc.nextInt() - 1;
		return p;
	}

	/**
	 * 
	 * @param p   La coordenada.
	 * @param mtx El taulell.
	 * @return Numero comprovant si hi ha una mina a la coordenada donada(9), en cas
	 *         contrari dona el número de mines del voltant(0-8).
	 */
	public static int[][] mostra(Posicio p, int[][] mtx, int[][] mtxE) {
		Posicio p2 = new Posicio();
		int num = 0;
		for (int i = p.f - 1; i <= p.f + 1; i++) {
			for (int j = p.c - 1; j <= p.c + 1; j++) {
				if ((!fora(mtx, i, j))) {
					if (mtx[i][j] == 1) {
						num++;
					}
				}
			}
		}
		if (num == 0) {
			mtxE[p.f][p.c] = 0;
			for (int i = p.f - 1; i <= p.f + 1; i++) {
				for (int j = p.c - 1; j <= p.c + 1; j++) {
					if ((!fora(mtx, i, j))) {
						if (mtxE[i][j]==9) {
							p2.f = i;
							p2.c = j;
							mtxE=mostra(p2, mtx, mtxE);
						}
					}
				}
			}
		} else {
			mtxE[p.f][p.c] = num;
		}

		return mtxE;
	}

	/**
	 * 
	 * @param mtx  El taulell intern indicant la posició de les mines
	 * @param mtx2 El taulell extern amb totes les opcions del usuari donades fins
	 *             ara.
	 * @return Si ha guanyat o no apartir de comprovar quines caselles 0 de la
	 *         primera matriu no són 9 a l'altre.
	 */
	public static boolean comprova_win(int[][] mtx, int[][] mtx2) {
		for (int i = 0; i < mtx2.length; i++) {
			for (int j = 0; j < mtx2.length; j++) {
				if (mtx[i][j] == 0 && mtx2[i][j] == 9) {
					return false;
				}
			}
		}
		return true;
	}
	public static void comprovacio(Posicio pos, boolean lose, boolean win,int[][] mtxI, int[][] mtxE) {
		if (mtxI[pos.f][pos.c] == 1) {
			lose = true;
		}else {
			mostra(pos, mtxI, mtxE);
			win = comprova_win(mtxI, mtxE);
		}
	}
	private static void inicialitzarGUI() {

		b.setColorbackground(0x000000); //posa el color de fons del background
		//el color està escrit en hexa RBG, és a dir, és un nombre hexadecimal que cada parella de 2 bits, de 00 a FF, representa el nombre RBG de 0 a 255. Per tant, 0xFF000 és vermell, 0x00FF00 és verd, i 0x0000FF és blau
		b.setActborder(true);  //fa que siguin visibles les vores entre caselles
		String[] lletres = { "", "1", "2", "3", "4", "5", "6", "7", "8", "*" };  //què s'ha d'escriure en cada casella en base al nombre. Per tant, per exemple, si el nombre de la matriu és 0 no s’escriurà res, si és 1 s’escriurà la string “1”, i si es 9 s’escriurà la String “*”
		b.setText(lletres);
		int[] colorlletres = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x521b98, 0xFFFFFF, 0xFF8000, 0x7F00FF }; //el color de les lletres, que veient com funcionava la String de lletres ja entendreu com va
		b.setColortext(colorlletres);
		String[] etiquetes2 = { "Mines: 10"};  //les etiquetes que es mostraran a la dreta de la pantalla. Haureu de canviar el camp de mines per la vostra variable
		w.setLabels(etiquetes2);
		w.setActLabels(true);
		w.setTitle("Cercamines");
		}
	/**
	 * 
	 * @param pos Posició que es demana a l'usuari al inici de cada ronda.
	 * @param p   Jugador que està jugant.
	 */
	public static void jugar(Posicio pos, Player p) {
		inicialitzarGUI();
		Boolean lose = false;
		Boolean win = false;
		int[][] mtxI = generar_ti();
		int[][] mtxE = generar_te();
		
		while (!lose && !win) {
			
			imprimir_tablero(mtxE);
			
			pos = demana(pos);
			comprovacio(pos, lose, win, mtxI, mtxE);
		}
		if (win) {
			System.out.println("HAS GUANYAT!!!!!!!!!! " + p.name + " GUANYADOR.");
			p.wins++;
			System.out.println();
		} else {
			System.out.println("BOOOOM!!! MINA TROBADA... HAS PERDUT");
			System.out.println();
		}
	}

	/**
	 * 
	 * @param ran El diccionari que emmagatzema el nom de tots els jugadors i
	 *            casacuna de les seves victories
	 * 
	 */
	public static void Ranking(LinkedHashMap<String, Integer> ran) {
		ArrayList<Entry<String, Integer>> list = new ArrayList<>(ran.entrySet());
		list.sort(Entry.comparingByValue());
		ListIterator li = list.listIterator(list.size());
		// Iterate in reverse.
		System.out.println("NOM DEL JUGAGOR = NOMBRE DE VICTORIES");
		System.out.println();
		while (li.hasPrevious()) {
			System.out.println(li.previous());
		}
	}
}

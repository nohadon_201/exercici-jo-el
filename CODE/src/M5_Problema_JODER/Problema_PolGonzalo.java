package M5_Problema_JODER;

import java.util.Scanner;

public class Problema_PolGonzalo {
	static Scanner sc = new Scanner(System.in);
	static int[][] mtx = rellenarlikeapavo();

	public static void main(String[] args) {
		int x = sc.nextInt();
		int y = sc.nextInt();
		int z = sc.nextInt();
		int k = sc.nextInt();

		System.out.println(doctor(1, x, y, z, k));
	}

	public static boolean fuera(int[][] mtx, int f, int c) {
		if (f > mtx.length - 1 || c > mtx.length - 1 || f < 0 || c < 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean apaño(int cas, int f, int c, int fc, int cc) {
		mtx = rellenarlikeapavo();
		return doctor(cas, f, c, fc, cc);
	}

	public static boolean doctor(int cas, int f, int c, int fc, int cc) {

		if (cas - 1 < 0) {
			if (!fuera(mtx, f + 1, c)) {
				if (mtx[f + 1][c] == 3) {
					return true;
				}
				mtx[f + 1][c] = 2;
			}
			if (!fuera(mtx, f - 1, c)) {
				if (mtx[f - 1][c] == 3) {
					return true;
				}
				mtx[f - 1][c] = 2;
			}
			if (!fuera(mtx, f, c + 1)) {
				if (mtx[f][c + 1] == 3) {
					return true;
				}
				mtx[f][c + 1] = 2;
			}
			if (!fuera(mtx, f, c - 1)) {
				if (mtx[f][c - 1] == 3) {
					return true;
				}
				mtx[f][c - 1] = 2;
			}
			return false;
		} else {
			mtx[fc][cc] = 3;
			if (mtx[f][c] == 3) {
				return true;
			}
			mtx[f][c] = 1;
			if (!fuera(mtx, f + 1, c)) {
				if (mtx[f + 1][c] == 3) {
					return true;
				}
				if (doctor(cas - 1, f + 1, c, 0, 0)) {
					return true;
				}
				mtx[f + 1][c] = 2;
			}
			if (!fuera(mtx, f - 1, c)) {
				if (mtx[f - 1][c] == 3) {
					return true;
				}
				if (doctor(cas - 1, f - 1, c, 0, 0)) {
					return true;
				}
				mtx[f - 1][c] = 2;
			}
			if (!fuera(mtx, f, c + 1)) {
				if (mtx[f][c + 1] == 3) {
					return true;
				}
				if (doctor(cas - 1, f, c + 1, 0, 0)) {
					return true;
				}
				mtx[f ][c + 1] = 2;
			}
			if (!fuera(mtx, f, c - 1)) {
				if (mtx[f][c - 1] == 3) {
					return true;
				}
				if (doctor(cas - 1, f, c - 1, 0, 0)) {
					return true;
				}
				mtx[f][c - 1] = 2;
			}
		}
		return false;
	}

	public static int[][] rellenarlikeapavo() {
		int[][] mtx = new int[10][10];
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				mtx[i][j] = 0;
			}
		}
		return mtx;
	}
}

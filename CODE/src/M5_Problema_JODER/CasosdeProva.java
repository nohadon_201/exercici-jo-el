package M5_Problema_JODER;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CasosdeProva {

	@Test
	void testPublic() {
		assertEquals(true, Problema_PolGonzalo.apaño(1, 3, 4, 1, 4));
		assertEquals(false, Problema_PolGonzalo.apaño(1, 3, 4, 1, 1));
		assertEquals(true, Problema_PolGonzalo.apaño(1, 3, 4, 4, 5));

	}
	@Test
	void dificil() {
		assertEquals(true, Problema_PolGonzalo.apaño(1, 0, 0, 0, 1));
		assertEquals(true, Problema_PolGonzalo.apaño(1, 0, 0, 0, 0));
		assertEquals(true, Problema_PolGonzalo.apaño(1, 9, 9, 9, 7));
	}
	

}
